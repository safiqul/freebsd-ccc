/*-
 * Copyright (c) 2015, 2016 Kristian A. Hiorth
 * Copyright (c) 2016, Øystein Dale
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/kernel.h>
#include <sys/malloc.h>
#include <sys/module.h>
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <sys/sysctl.h>
#include <sys/systm.h>
#include <sys/ktr.h>
#include <sys/kthread.h>
#include <sys/lock.h>
#include <sys/mutex.h>
#include <sys/param.h>
#include <sys/unistd.h>

#include <net/vnet.h>

#include <netinet/in.h>
#include <netinet/in_pcb.h>
#include <netinet/tcp_var.h>
#include <netinet/tcp_seq.h>
#include <netinet/cc/cc.h>
#include <netinet/cc/cc_module.h>

// File ops
#include <sys/syscallsubr.h>
#include <sys/fcntl.h>

#include <netinet/cc/cc_fse.h>

#include <sys/sched.h>

static VNET_DEFINE(struct fse_state, cc_fse_state);
#define V_cc_fse_state VNET(cc_fse_state)

static VNET_DEFINE(int, cc_fse_fgbuckets) = 2048;
#define V_cc_fse_fgbuckets VNET(cc_fse_fgbuckets)

static VNET_DEFINE(struct callout, cc_fse_reaper_callout);
#define V_cc_fse_reaper_callout VNET(cc_fse_reaper_callout)

static VNET_DEFINE(int, cc_fse_reaper_ival) = 90;
#define V_cc_fse_reaper_ival VNET(cc_fse_reaper_ival)

static VNET_DEFINE(int, cc_fse_reaper_limit) = 180;
#define V_cc_fse_reaper_limit VNET(cc_fse_reaper_limit)

static VNET_DEFINE(int, cc_fse_shared_aggregate) = 0;
#define V_cc_fse_shared_aggregate VNET(cc_fse_shared_aggregate)

static VNET_DEFINE(int, cc_fse_aggregate_idx) = 0;
#define V_cc_fse_aggregate_idx VNET(cc_fse_aggregate_idx)

VNET_DEFINE(uint8_t, cc_fse_default_prio) = FSE_DEFAULT_PRIORITY;

VNET_DEFINE(uint8_t, cc_rd) = FSE_DEFAULT_PRIORITY;
VNET_DEFINE(uint8_t, cc_wr) = FSE_DEFAULT_PRIORITY;

static MALLOC_DEFINE(M_FSE, "FSE data",
		     "FSE congestion control data");

static struct fse_fg* cc_fse_lookup_addrs(struct in_addr faddr, struct in_addr laddr);
static struct fse_fg* cc_fse_lookup_id(uint32_t id);
static void cc_fse_reaper(void *arg);

void cc_fse_reaper(void *arg);

static int read_aggregate(u_long *aggregate, u_long *count);
static int write_aggregate(u_long aggregate, u_long count);
static int aggregate_lock();
static int aggregate_unlock();
static int read_file(char* path, u_long *aggregate);
static int write_file(char* path, u_long aggregate);

#if 0
#define PRINTF(fmt, args...) do { printf("FSE: "); printf(fmt, args); printf("\n"); } while (0);
#define PRINT(str) do { printf("FSE: %s\n", str); } while (0);
#else
#define PRINTF(fmt, ...) do {} while (0);
#define PRINT(str) do {} while (0);
#endif

// The sum of the two numbers in the aggregate file
static u_long s_shared_aggregate_sum = 0;

// The sum of local aggregates on this VM
static u_long s_local_aggregate_sum = 0;

static int aggregate_idx = 0;

static struct thread *td = NULL;

static unsigned int enabled = 0;

static void
update_thread(void *ptr)
{
	char *aggregate_file1 = "/mnt/share/aggregate1";
	char *aggregate_file2 = "/mnt/share/aggregate2";

	u_long remote_aggregate = 0;
	const int idx = atomic_load_acq_int(&aggregate_idx);

	EVENTHANDLER_REGISTER(shutdown_pre_sync, kthread_shutdown, curthread, SHUTDOWN_PRI_LAST);

	for (;;) {
		kthread_suspend_check();

		// Update local aggregate sum
		if (idx) {
			// aggregate2 = atomic_load_acq_long(&s_local_aggregate_sum);
			read_file(aggregate_file2, &remote_aggregate);
			PRINTF("Read file %s, value was %lu", aggregate_file2, remote_aggregate);
		} else {
			// aggregate1 = atomic_load_acq_long(&s_local_aggregate_sum);
			read_file(aggregate_file1, &remote_aggregate);
			PRINTF("Read file %s, value was %lu", aggregate_file1, remote_aggregate);
		}

		// Write shared aggregate sum
		// s_shared_aggregate_sum = aggregate2 + aggregate1;
		atomic_store_rel_long(&s_shared_aggregate_sum, s_local_aggregate_sum + remote_aggregate);
		PRINTF("Aggregate sum is %lu", s_local_aggregate_sum + remote_aggregate);

		if (idx) {
			// aggregate2 = atomic_load_acq_long(&s_local_aggregate_sum);
			write_file(aggregate_file1, s_local_aggregate_sum);
		} else {
			// aggregate1 = atomic_load_acq_long(&s_local_aggregate_sum);
			write_file(aggregate_file2, s_local_aggregate_sum);
		}
	}

	kthread_exit();
}

static void
cc_fse_init(void)
{
	PRINT("Flow State Exchange initializing...\n");

	rw_init(&V_cc_fse_state.fse_statelock, "FSE global state lock");
	V_cc_fse_state.fse_fgtable = hashinit(V_cc_fse_fgbuckets, M_FSE,
					      &V_cc_fse_state.fse_fgtable_mask);

	callout_init(&V_cc_fse_reaper_callout, CALLOUT_MPSAFE);
	callout_reset(&V_cc_fse_reaper_callout, V_cc_fse_reaper_ival * hz,
		      cc_fse_reaper, curvnet);
}

/* lookup based on destination address */
static struct fse_fg*
cc_fse_lookup_addrs(struct in_addr faddr, struct in_addr laddr)
{
	struct fse_fg_head *head;
	struct fse_fg *fg;
	struct fse_flow *first;
	FSE_STATE_LOCK_ASSERT(V_cc_fse_state);

	head = &V_cc_fse_state.fse_fgtable[ \
		FSE_FG_ADDRHASH(faddr.s_addr, laddr.s_addr, V_cc_fse_state.fse_fgtable_mask)];
	LIST_FOREACH(fg, head, fg_fgte) {
		FSE_FG_MBR_RLOCK(fg);
		first = LIST_FIRST(&fg->fg_members);
		/*
		 * FIXME reading from INPCB without a lock. Can I get
		 * away with it? Should I just copy the addrs instead?
		 */
		if (first != NULL
		    && first->f_inp->inp_faddr.s_addr == faddr.s_addr
		    && first->f_inp->inp_laddr.s_addr == laddr.s_addr) {
			FSE_FG_MBR_UNLOCK(fg);
			return fg;
		}
		FSE_FG_MBR_UNLOCK(fg);
	}

	return NULL;
}

static struct fse_fg*
cc_fse_lookup_id(uint32_t id)
{
	struct fse_fg_head *head;
	struct fse_fg *fg;
	FSE_STATE_LOCK_ASSERT(V_cc_fse_state);

	head = &V_cc_fse_state.fse_fgtable[id % V_cc_fse_fgbuckets];

	LIST_FOREACH(fg, head, fg_fgte) {
		if (fg->fg_id == id)
			return fg;
	}

	return NULL;
}

/* Write cwnd, ssthresh back to TCB, clamping values */
static inline void
cc_fse_writeback(struct fse_flow *f)
{
	struct tcpcb *tcb;

	tcb = intotcpcb(f->f_inp);

	tcb->snd_cwnd = max(f->f_cwnd, tcb->t_maxseg);
	tcb->snd_ssthresh = max(f->f_ssthresh, tcb->t_maxseg * 2);
}

static void
cc_fse_couple(struct fse_flow *f)
{
	struct fse_fg *fg;
	struct tcpcb *tcb;

	if (enabled == 0) {
		int v = V_cc_fse_aggregate_idx;

		atomic_set_rel_int(&enabled, 1);
		atomic_set_rel_int(&aggregate_idx, v);

		if (kthread_add(&update_thread, NULL, NULL, &td, RFNOWAIT, 0, "update_thread"))
			KASSERT(0, ("kthread_add failed"));
	} else if (enabled == 2) {
		kthread_resume(td);
	}

	tcb = intotcpcb(f->f_inp);

	fg = f->f_fg;
	FSE_FG_MBR_WLOCK_ASSERT(fg);

	fg->fg_s_p += f->f_p;
	fg->fg_num_coupled += 1;

	if (fg->fg_num_coupled == 1) {
		if (V_cc_fse_shared_aggregate) {
			s_local_aggregate_sum = tcb->snd_cwnd;
		} else {
			fg->fg_s_cwnd = tcb->snd_cwnd;
		}

		fg->fg_s_ssthresh = 0;

		fg->fg_coco = f;
	}

	if (fg->fg_num_coupled == 1) {
		PRINT("First flow in group");
		/*
		 * FG inherits cwnd/ssthresh of first flow coupled to
		 * it.
		 * If an FG went stale, but is revived, we reuse the
		 * old params. This is temporal sharing.
		 */
		if (fg->fg_s_cwnd == 0) {
			fg->fg_s_cwnd = tcb->snd_cwnd;
			fg->fg_s_ssthresh = 0;
		}
		fg->fg_coco = f;
	} else {
		PRINT("Not first flow in group");
	}

	f->f_cwnd = f->f_p * fg->fg_s_cwnd / fg->fg_s_p;
	f->f_ssthresh = tcb->snd_ssthresh;

	if (fg->fg_s_ssthresh > 0)
		f->f_ssthresh = f->f_p * fg->fg_s_ssthresh / fg->fg_s_p;

	cc_fse_writeback(f);

	f->f_state = FSE_FLOW_COUPLED;

	PRINTF("cc_fse_couple(%p): flow %lu coupled to fg %d. f_cwnd=%lu CoCo=%lu (%p) members=%u",
	     f, f->f_fi, fg->fg_id, fg->fg_coco->f_fi, f->f_cwnd, fg->fg_coco, fg->fg_num_coupled);
}

static void
cc_fse_decouple(struct fse_flow *f)
{
	struct fse_fg *fg;
	struct fse_flow *fg_f, *candidate;
	struct tcpcb *tp;

	if (!(f->f_state & FSE_FLOW_COUPLED))
		return;

	fg = f->f_fg;
	FSE_FG_MBR_WLOCK_ASSERT(fg);

	fg->fg_s_p -= f->f_p;
	fg->fg_num_coupled -= 1;

	if (fg->fg_coco == f) {
		/*
		 * Select new CoCo:
		 * Pick the first flow in CA we find. If there are
		 * none, we prefer a flow in FR over one in SS.
		 */
		candidate = NULL;
		LIST_FOREACH(fg_f, &fg->fg_members, f_fge) {
			if (fg_f->f_state & FSE_FLOW_UNCOUPLED)
				continue;

			tp = intotcpcb(fg_f->f_inp);
			if (candidate != NULL && (fg_f->f_state & FSE_FLOW_WANTED_SS)) {
				/* Skip SS flows except if we have
				 * nothing better */
				continue;
			}

			candidate = fg_f;

			/* Flow in CA; it will do! */
			if (!(fg_f->f_state & FSE_FLOW_IN_FR))
				break;
		}

		fg->fg_coco = candidate;
		PRINTF("%s: new CoCo=%lu (%p)", __func__,
		     (candidate != NULL) ? candidate->f_fi : -1UL, candidate);
	}

	if (fg->fg_num_coupled == 0) {
		kthread_suspend(td, 100);
		enabled = 2;
	}

	PRINTF("%s(%p): flow %lu decoupled from fg %p. CoCo=%lu (%p)",
	     __func__, f, f->f_fi, fg, (fg->fg_coco != NULL) ? fg->fg_coco->f_fi : -1,
	     fg->fg_coco);
}

static struct fse_fg*
cc_fse_group_by_addrs(struct fse_flow *f)
{
	struct fse_fg *fg;
	struct fse_fg_head *fgt_head;
	struct tcpcb *tp;

	PRINTF("%s(%lu)", __func__, f->f_fi);

	FSE_STATE_WLOCK_ASSERT(V_cc_fse_state);

	INP_LOCK_ASSERT(f->f_inp);
	tp = intotcpcb(f->f_inp);
	fg = cc_fse_lookup_addrs(f->f_inp->inp_faddr, f->f_inp->inp_laddr);
	if (fg == NULL) {
		fg = malloc(sizeof(struct fse_fg), M_FSE, M_NOWAIT|M_ZERO);

		if (fg == NULL) {
			PRINT("cc_fse_group: Could not allocate memory for new FSE FG!\n");
			return NULL;
		}

		rw_init_flags(&fg->fg_memberslock, "FSE FG lock", RW_RECURSE);
		LIST_INIT(&fg->fg_members);

		fgt_head = &V_cc_fse_state.fse_fgtable[ \
			FSE_FG_ADDRHASH(f->f_inp->inp_faddr.s_addr,
					f->f_inp->inp_laddr.s_addr,
					V_cc_fse_state.fse_fgtable_mask)];
		LIST_INSERT_HEAD(fgt_head, fg, fg_fgte);

		PRINTF("Created flow group %u", f->f_group_id);
	}

	FSE_FG_MBR_WLOCK(fg);
	LIST_INSERT_HEAD(&fg->fg_members, f, f_fge);
	fg->fg_membercount += 1;
	f->f_fg = fg;

	cc_fse_couple(f);

	getmicrouptime(&fg->fg_lastupdate);
	FSE_FG_MBR_UNLOCK(fg);

	return fg;
}

static struct fse_fg*
cc_fse_group_by_id(struct fse_flow *f)
{
	struct fse_fg *fg;
	struct fse_fg_head *fgt_head;
	struct tcpcb *tp;

	PRINTF("%s(%lu)", __func__, f->f_fi);

	FSE_STATE_WLOCK_ASSERT(V_cc_fse_state);

	INP_LOCK_ASSERT(f->f_inp);
	tp = intotcpcb(f->f_inp);
	fg = cc_fse_lookup_id(f->f_group_id);
	if (fg == NULL) {
		fg = malloc(sizeof(struct fse_fg), M_FSE, M_NOWAIT|M_ZERO);

		if (fg == NULL) {
			PRINT("cc_fse_group: Could not allocate memory for new FSE FG!\n");
			return NULL;
		}

		rw_init_flags(&fg->fg_memberslock, "FSE FG lock", RW_RECURSE);
		LIST_INIT(&fg->fg_members);
		fg->fg_id = f->f_group_id;

		fgt_head = &V_cc_fse_state.fse_fgtable[f->f_group_id % V_cc_fse_fgbuckets];
		LIST_INSERT_HEAD(fgt_head, fg, fg_fgte);

		PRINTF("Created flow group %u", f->f_group_id);
	}

	FSE_FG_MBR_WLOCK(fg);
	LIST_INSERT_HEAD(&fg->fg_members, f, f_fge);
	fg->fg_membercount += 1;
	f->f_fg = fg;

    cc_fse_couple(f);

	getmicrouptime(&fg->fg_lastupdate);
	FSE_FG_MBR_UNLOCK(fg);
	return fg;
}

struct fse_flow*
cc_fse_register(struct inpcb *inp)
{
	struct fse_flow *flow;
	struct tcpcb *tcb;

	PRINTF("cc_fse_register(%p)", inp);

	flow = malloc(sizeof(struct fse_flow), M_FSE, M_NOWAIT|M_ZERO);

	if (flow == NULL)
		goto out;

	tcb = intotcpcb(inp);

	flow->f_p = tcb->t_fse_prio;
	flow->f_inp = inp;
	flow->f_group_id = tcb->t_fse_group;

	FSE_STATE_WLOCK(V_cc_fse_state);
	flow->f_fi = V_cc_fse_state.fse_next_fid++;

	/* The rest of the flow data MUST be set BEFORE grouping! */
#if 1
	cc_fse_group_by_id(flow);
#else
	cc_fse_group_by_addrs(flow);
#endif

    PRINT("cc_fse_register done");

	FSE_STATE_UNLOCK(V_cc_fse_state);
 out:
	return flow;
}

void
cc_fse_deregister(struct fse_flow *f)
{
	struct fse_fg *fg;

	fg = f->f_fg;

    if (fg == NULL)
        goto end;

	FSE_FG_MBR_WLOCK(fg);
	LIST_REMOVE(f, f_fge);
	fg->fg_membercount -= 1;
	cc_fse_decouple(f);
	getmicrouptime(&fg->fg_lastupdate);

	PRINTF("cc_fse_deregister: dropped flow %lu (%p), tcb=%p, fg=%p (%u/%u flows)",
	     f->f_fi, f, intotcpcb(f->f_inp), f->f_fg, f->f_fg->fg_num_coupled,
	     f->f_fg->fg_membercount);
	/* XXX Empty FGs should be purged, but not immediately */
	FSE_FG_MBR_UNLOCK(fg);

end:
	free(f, M_FSE);
}

/*
 * TODO: There might still be issues if the result of the write is cached on the
 * VM, but not propagated to the host.
 * Consider using O_DIRECT if this turns out to be the case.
 */
static int
aggregate_lock()
{
	// Try to create the file
	// If it exists, then wait and re-run
	int error, rc;

	do {
		error = kern_openat(curthread, AT_FDCWD, "/mnt/share/lock", UIO_SYSSPACE, O_RDONLY | O_CREAT | O_EXCL | O_DIRECT, 0);
	} while (error != 0 && error == EEXIST);

	rc = curthread->td_retval[0];

	if (error != 0 && error != EEXIST) {
		PRINTF("%s(): error opening lock file: error: %d rc: %d", __func__, error, rc);
	}

	kern_close(curthread, rc); // rc == fd

	return 0;
}

static int
aggregate_unlock()
{
	int rc, error;

	// Unlink file if it exists, otherwise ignore it
	if ((error = kern_unlinkat(curthread, AT_FDCWD, "/mnt/share/lock", UIO_SYSSPACE, 0)) != 0) {
		rc = curthread->td_retval[0];
		PRINTF("%s(): ERROR: Unable to unlink lock file, error: %d rc: %d", __func__, error, rc);
		return -1;
	}

	return 0;
}

static int
read_aggregate(u_long *aggregate, u_long *count)
{
	int fd, rc, error;
	struct uio auio;
	struct iovec aiov;
	char buffer[128];

	bzero(&aiov, sizeof(aiov));
	bzero(&auio, sizeof(auio));
	bzero(buffer, sizeof(buffer));

	error = kern_openat(curthread, AT_FDCWD, "/mnt/share/aggregate", UIO_SYSSPACE, O_RDWR, 0);

	if (error) {
		PRINTF("%s(): error opening aggregate file: %d", __func__, error);
		return -1;
	}

	fd = curthread->td_retval[0];

	aiov.iov_base = buffer;
	aiov.iov_len = 127;
	auio.uio_iov = &aiov;
	auio.uio_iovcnt = 1;
	auio.uio_offset = 0;
	auio.uio_rw = UIO_READ;
	auio.uio_resid = 127;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_td = curthread;

	error = kern_readv(curthread, fd, &auio);
	rc = curthread->td_retval[0];

	if (error) {
		PRINTF("%s(): Error reading aggregate file: err: %d, rc: %d", __func__, error, rc);
		kern_close(curthread, fd);
		return -1;
	}

	PRINTF("%s(): Read length: %d  buffer: %s  rc: %d", __func__, error, buffer, rc);

	if (sscanf(buffer, "%lu %lu", &aggregate, &count) != 2) {
		PRINTF("%s(): Error parsing aggregate, rc: %d", __func__, rc);
		return -1;
	}

	kern_close(curthread, fd);

	return 0;
}

static int
write_aggregate(u_long aggregate, u_long count)
{
	int fd, error;
	struct uio auio;
	struct iovec aiov;
	char buffer[128];
	size_t len;

	bzero(&aiov, sizeof(aiov));
	bzero(&auio, sizeof(auio));
	bzero(buffer, sizeof(buffer));

	error = kern_openat(curthread, AT_FDCWD, "/mnt/share/aggregate", UIO_SYSSPACE, O_RDWR | O_CREAT | O_TRUNC, 0);

	if (error) {
		PRINTF("%s(): error opening aggregate file: %d", __func__, error);
		return -1;
	}

	fd = curthread->td_retval[0];

	len = sprintf(buffer, "%lu %lu", aggregate, count);

	auio.uio_iov = &aiov;
	auio.uio_rw = UIO_WRITE;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_td = curthread;

	size_t written = 0;
	do {
		aiov.iov_base = buffer + written;
		aiov.iov_len = len - written;
		auio.uio_iovcnt = 1;
		auio.uio_offset = written;
		auio.uio_resid = len - written;

		error = kern_writev(curthread, fd, &auio);

		if (error)
			break;
		else
			written += curthread->td_retval[0];
	} while (written < len);

	if (error) {
		PRINTF("%s(): Error writing aggregate file: %d", __func__, error);
		kern_close(curthread, fd);
		return -1;
	}

	PRINTF("%s(): Wrote %lu bytes to the aggregate file", __func__, len);

	kern_close(curthread, fd);

	return 0;
}

static int
read_file(char* path, u_long *aggregate)
{
	int fd, rc, error;
	struct uio auio;
	struct iovec aiov;
	char buffer[128];

	bzero(&aiov, sizeof(aiov));
	bzero(&auio, sizeof(auio));
	bzero(buffer, sizeof(buffer));

	error = kern_openat(curthread, AT_FDCWD, path, UIO_SYSSPACE, O_RDWR, 0);

	if (error) {
		PRINTF("%s(): error opening aggregate file: %d", __func__, error);
		return -1;
	}

	fd = curthread->td_retval[0];

	aiov.iov_base = buffer;
	aiov.iov_len = 127;
	auio.uio_iov = &aiov;
	auio.uio_iovcnt = 1;
	auio.uio_offset = 0;
	auio.uio_rw = UIO_READ;
	auio.uio_resid = 127;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_td = curthread;

	error = kern_readv(curthread, fd, &auio);
	rc = curthread->td_retval[0];

	if (error) {
		PRINTF("%s(): Error reading aggregate file: err: %d, rc: %d", __func__, error, rc);
		kern_close(curthread, fd);
		return -1;
	}

	PRINTF("%s(): Read length: %d  buffer: %s  rc: %d", __func__, error, buffer, rc);

	if (sscanf(buffer, "%lu", aggregate) != 1) {
		PRINTF("%s(): Error parsing aggregate, rc: %d", __func__, rc);
		return -1;
	}

	kern_close(curthread, fd);

	return 0;
}

static int
write_file(char* path, u_long aggregate)
{
	int fd, error;
	struct uio auio;
	struct iovec aiov;
	char buffer[128];
	size_t len;

	bzero(&aiov, sizeof(aiov));
	bzero(&auio, sizeof(auio));
	bzero(buffer, sizeof(buffer));

	error = kern_openat(curthread, AT_FDCWD, path, UIO_SYSSPACE, O_RDWR | O_CREAT | O_TRUNC, 0);

	if (error) {
		PRINTF("%s(): error opening aggregate file: %d", __func__, error);
		return -1;
	}

	fd = curthread->td_retval[0];

	len = sprintf(buffer, "%lu", aggregate);

	auio.uio_iov = &aiov;
	auio.uio_rw = UIO_WRITE;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_td = curthread;

	size_t written = 0;
	do {
		aiov.iov_base = buffer + written;
		aiov.iov_len = len - written;
		auio.uio_iovcnt = 1;
		auio.uio_offset = written;
		auio.uio_resid = len - written;

		error = kern_writev(curthread, fd, &auio);

		if (error)
			break;
		else
			written += curthread->td_retval[0];
	} while (written < len);

	if (error) {
		PRINTF("%s(): Error writing aggregate file: %d", __func__, error);
		kern_close(curthread, fd);
		return -1;
	}

	PRINTF("%s(): Wrote %lu bytes to the aggregate file", __func__, len);

	kern_close(curthread, fd);

	return 0;
}

void
cc_fse_update(struct fse_flow *f, uint32_t flags, uint32_t incr)
{
	struct tcpcb *tcb;
	struct fse_fg *fg;
	struct fse_flow *fg_f;
	u_long cc_cwnd, cc_ssthresh;

	KASSERT(f != NULL, ("updating a NULL flow"));
	INP_WLOCK_ASSERT(f->f_inp);

	tcb = intotcpcb(f->f_inp);
	fg = f->f_fg;
	cc_cwnd = tcb->snd_cwnd;
	cc_ssthresh = tcb->snd_ssthresh;

	PRINTF("%s(%p, %u) flow %lu tcb=%p", __func__, f, flags, f->f_fi, tcb);

	FSE_FG_MBR_WLOCK(fg);

	if (f->f_state & FSE_FLOW_COUPLED) {

		/* Check if CC algo tried to go into SS */
		if (cc_cwnd < cc_ssthresh)
			f->f_state |= FSE_FLOW_WANTED_SS;
		else
			f->f_state &= ~FSE_FLOW_WANTED_SS;

		/* Check if flow is currently in FR */
		if (IN_RECOVERY(tcb->t_flags))
			f->f_state |= FSE_FLOW_IN_FR;
		else
			f->f_state &= ~FSE_FLOW_IN_FR;

		if (fg->fg_coco != f) {
			// REF OYSTEDAL: A joining flow should not be CoCo
			//if (f->f_clocking_state == FSE_FLOW_JOINING && IN_FASTRECOVERY(tcb->t_flags)) {
			if (IN_FASTRECOVERY(tcb->t_flags)) {
				/* Search for any other flows not in CA */
				int non_ca = 0;
				LIST_FOREACH(fg_f, &fg->fg_members, f_fge) {
					if (fg_f->f_state & FSE_FLOW_IN_FR) {
						non_ca = 1;
						break;
					}
				}

				/* Everyone else in CA, become CoCo */
				if (!non_ca) {
					fg->fg_coco = f;
					PRINTF("%s: FR flow %p taking over as CoCo.",
					     __func__, f);
				}
			} else {
				u_long share;

				if (V_cc_fse_shared_aggregate) {
					atomic_store_rel_long(&s_local_aggregate_sum, fg->fg_s_cwnd);
					share = f->f_p * (atomic_load_acq_long(&s_shared_aggregate_sum) / 2) / fg->fg_s_p;
				} else {
					share = f->f_p * fg->fg_s_cwnd / fg->fg_s_p;
				}

				f->f_cwnd = share;

				if (fg->fg_s_ssthresh > 0)
					f->f_ssthresh = f->f_p * fg->fg_s_ssthresh / fg->fg_s_p;

				PRINTF("f %lu cwnd=%lu ssthr=%lu", f->f_fi, f->f_cwnd, f->f_ssthresh);

				cc_fse_writeback(f);
				goto out;
			}
		}

		/* CoCo might have changed above */
		if (fg->fg_coco == f) {
			PRINTF("f %lu is CoCo; f_cwnd=%lu, f_sst=%lu, s_cwnd=%lu, " \
			     "s_sst=%lu, f_p=%u, s_p=%u.",
			     f->f_fi, f->f_cwnd, f->f_ssthresh, fg->fg_s_cwnd, fg->fg_s_ssthresh,
			     f->f_p, fg->fg_s_p);

			if (!f->f_ssbit && !IN_RECOVERY(tcb->t_flags)) {
				u_long share;

				PRINT("CA");

				PRINTF("f %lu PRE cwnd=%lu ssthr=%lu", f->f_fi, f->f_cwnd, f->f_ssthresh);
				PRINTF("fg %u PRE cwnd=%lu ssthr=%lu", fg->fg_id, fg->fg_s_cwnd, fg->fg_s_ssthresh);

				/* Normal CA update */
				if (cc_cwnd >= f->f_cwnd) { /* AI... */
					fg->fg_s_cwnd += cc_cwnd - f->f_cwnd;
				} else { /* ..MD */
					/* XXX Operand order significant! */
					fg->fg_s_cwnd = fg->fg_s_cwnd * cc_cwnd / f->f_cwnd;
				}

				if (V_cc_fse_shared_aggregate) {
					atomic_store_rel_long(&s_local_aggregate_sum, fg->fg_s_cwnd);
					share = f->f_p * (atomic_load_acq_long(&s_shared_aggregate_sum) / 2) / fg->fg_s_p;
				} else {
					share = f->f_p * fg->fg_s_cwnd / fg->fg_s_p;
				}

				PRINTF("fg %u cwnd=%lu", fg->fg_id, fg->fg_s_cwnd);

				// Set the cwnd of this flow to its share
				f->f_cwnd = share;

				f->f_ssthresh = cc_ssthresh;
				if (fg->fg_s_ssthresh > 0)
					f->f_ssthresh = f->f_p * fg->fg_s_ssthresh / fg->fg_s_p;

				PRINTF("f %lu POST cwnd=%lu ssthr=%lu", f->f_fi, f->f_cwnd, f->f_ssthresh);
				PRINTF("fg %u POST cwnd=%lu ssthr=%lu", fg->fg_id, fg->fg_s_cwnd, fg->fg_s_ssthresh);

				cc_fse_writeback(f);
			} else if (IN_FASTRECOVERY(tcb->t_flags)) {
				PRINT("FR");
				PRINTF("f %lu PRE cwnd=%lu ssthr=%lu", f->f_fi, f->f_cwnd, f->f_ssthresh);
				PRINTF("fg %u PRE cwnd=%lu ssthr=%lu", fg->fg_id, fg->fg_s_cwnd, fg->fg_s_ssthresh);

				if (V_cc_fse_shared_aggregate) {
					fg->fg_s_ssthresh = atomic_load_acq_long(&s_shared_aggregate_sum) / 4;
				} else {
					fg->fg_s_ssthresh = fg->fg_s_cwnd / 2;
				}

				PRINTF("f %lu POST cwnd=%lu ssthr=%lu", f->f_fi, f->f_cwnd, f->f_ssthresh);
				PRINTF("fg %u POST cwnd=%lu ssthr=%lu", fg->fg_id, fg->fg_s_cwnd, fg->fg_s_ssthresh);
			} else if (f->f_state & FSE_FLOW_WANTED_SS) {
				PRINT("SS");

				PRINTF("f %lu PRE cwnd=%lu ssthr=%lu", f->f_fi, f->f_cwnd, f->f_ssthresh);
				PRINTF("fg %u PRE cwnd=%lu ssthr=%lu", fg->fg_id, fg->fg_s_cwnd, fg->fg_s_ssthresh);

				// Set SS bit of flow to 1 if we're in timeout
				if (f->f_timeout) {
					f->f_ssbit = 1;
				}

				// Check if all are in SS
				LIST_FOREACH(fg_f, &fg->fg_members, f_fge) {
					if (!f->f_ssbit) {
						break;
					}
				}

				// fg_f == NULL => all in SS
				if (fg_f == NULL) {
					PRINT("All in SS");

					//   Flip SS bits
					LIST_FOREACH(fg_f, &fg->fg_members, f_fge) {
						fg_f->f_ssbit = 0;
						fg_f->f_timeout = 0;
					}

					if (V_cc_fse_shared_aggregate) {
						atomic_store_rel_long(&s_local_aggregate_sum, fg->fg_s_cwnd);
						f->f_cwnd = f->f_p * (atomic_load_acq_long(&s_shared_aggregate_sum) / 2) / fg->fg_s_p;

						//   Perform update of ssthresh
						fg->fg_s_ssthresh = atomic_load_acq_long(&s_shared_aggregate_sum) / 4;
					} else {
						/* XXX Order of
						 * operands is
						 * significant here!!! */
						fg->fg_s_cwnd = fg->fg_s_cwnd * cc_cwnd / f->f_cwnd;
						f->f_cwnd = f->f_p * fg->fg_s_cwnd / fg->fg_s_p;

						//   Perform update of ssthresh
						fg->fg_s_ssthresh = fg->fg_s_cwnd / 2;
					}

					/* FIXME What about ssthresh?!
					 * Can't use writeback here?!*/
					tcb->snd_cwnd = max(f->f_cwnd, tcb->t_maxseg);
				} else {
					fg->fg_coco = fg_f;
					PRINTF("%s: old coco in SS, elected %lu",
						__func__, fg_f->f_fi);

					/* XXX Shouldn't we write
					 * back old values?
					 * NOT IN DRAFT */
					cc_fse_writeback(f);
				}

				PRINTF("f %lu POST cwnd=%lu ssthr=%lu", f->f_fi, f->f_cwnd, f->f_ssthresh);
				PRINTF("fg %u POST cwnd=%lu ssthr=%lu", fg->fg_id, fg->fg_s_cwnd, fg->fg_s_ssthresh);
			}
		}
	}

 out:
	getmicrouptime(&fg->fg_lastupdate);
	FSE_FG_MBR_UNLOCK(fg);

	PRINT("END\n");
}

/*
 * Remove and clean up after a FG.
 * Caller must hold both state and FG Wlock.
 * Releases the FG wlock.
 */
static void
cc_fse_fg_destroy(struct fse_fg *fg)
{
	FSE_STATE_WLOCK_ASSERT(V_cc_fse_state);
	FSE_FG_MBR_WLOCK_ASSERT(fg);
	KASSERT(fg->fg_membercount == 0,
		("Attempting to destroy non-empty FSE FG!"));

	/*
	 * There should be no race wrt. threads being blocked on this
	 * lock at this point, they would need a pointer via some
	 * flow, and we just checked there were none in this FG. No
	 * new ones can be added while we hold the state lock.
	 */
	FSE_FG_MBR_UNLOCK(fg);

	LIST_REMOVE(fg, fg_fgte);

	rw_destroy(&fg->fg_memberslock);

	free(fg, M_FSE);
}

/*
 * Walk the FG hashtable and reap stale empty FGs.
 * Called by periodic callout.
 */
static void
cc_fse_reaper(void *arg)
{
	struct fse_fg *fg, *fg_temp;
	struct timeval now;
	int i;

	CURVNET_SET((struct vnet *) arg);

	getmicrouptime(&now);

	FSE_STATE_WLOCK(V_cc_fse_state);
	for (i = 0; i < V_cc_fse_state.fse_fgtable_mask; i++) {
		LIST_FOREACH_SAFE(fg, &V_cc_fse_state.fse_fgtable[i], fg_fgte, fg_temp) {
			/*
			 * XXX If we get smarter about FG locking, it
			 * would make sense to check under Rlock and
			 * upgrade to Wlock only if we need to reap.
			 */
			FSE_FG_MBR_WLOCK(fg);
			if (fg->fg_membercount == 0
			    && (now.tv_sec - fg->fg_lastupdate.tv_sec > V_cc_fse_reaper_limit)) {
				cc_fse_fg_destroy(fg);
				/* fg_destroy cleans the lock: */
				continue;
			}
			FSE_FG_MBR_UNLOCK(fg);
		}
	}
	FSE_STATE_UNLOCK(V_cc_fse_state);

	callout_reset(&V_cc_fse_reaper_callout, V_cc_fse_reaper_ival * hz,
		      cc_fse_reaper, curvnet);

	CURVNET_RESTORE();
}

/*
 * Get or set the default priority for this VNET.
 * Needs a handler procedure due to input validation.
 */
static int
cc_fse_default_prio_handler(SYSCTL_HANDLER_ARGS)
{
	int error = 0;
	int value = V_cc_fse_default_prio;

	error = sysctl_handle_int(oidp, &value, 0, req);

	if (error != 0)
		return error;

	if (value < 1 || value > 255 || value % 2)
		return EINVAL;

	V_cc_fse_default_prio = value;

	return 0;
}

static int
sysctl_aggregate_read(SYSCTL_HANDLER_ARGS)
{
	int error;
	int value;
	u_long aggregate;
	u_long count = 0;

	aggregate_lock();

	if (read_aggregate(&aggregate, &count)) {
		aggregate_unlock();
		return -1;
	}

	value = (int)aggregate;

	error = sysctl_handle_int(oidp, &value, 0, req);

	if (error || !req->newptr) {
		aggregate_unlock();
		return error;
	}

	aggregate = (u_long)value;
	count++;

	if (write_aggregate(aggregate, count)) {
		aggregate_unlock();
		return -1;
	}

	aggregate_unlock();
	return 0;
}

static int
sysctl_aggregate_write(SYSCTL_HANDLER_ARGS)
{
	return 0;
}



/* Init hook */
VNET_SYSINIT(cc_fse, SI_SUB_LAST, SI_ORDER_ANY, cc_fse_init, NULL);

/* Define sysctl subtree for our config */

SYSCTL_NODE(_net_inet_tcp_cc, OID_AUTO, fse, CTLFLAG_RW, NULL,
	    "Flow State Exchange");

SYSCTL_INT(_net_inet_tcp_cc_fse, OID_AUTO, fgbuckets, CTLFLAG_RDTUN | CTLFLAG_VNET,
	   &VNET_NAME(cc_fse_fgbuckets), 0, "FG hashtable buckets");

SYSCTL_INT(_net_inet_tcp_cc_fse, OID_AUTO, reaper_ival, CTLFLAG_RW | CTLFLAG_VNET,
	   &VNET_NAME(cc_fse_reaper_ival), 0, "FG reaper interval");

SYSCTL_INT(_net_inet_tcp_cc_fse, OID_AUTO, reaper_limit, CTLFLAG_RW | CTLFLAG_VNET,
	   &VNET_NAME(cc_fse_reaper_limit), 0, "FG idle timeout");

SYSCTL_INT(_net_inet_tcp_cc_fse, OID_AUTO, shared_aggregate, CTLFLAG_RW | CTLFLAG_VNET,
	   &VNET_NAME(cc_fse_shared_aggregate), 0, "Enable aggregate sharing");

SYSCTL_INT(_net_inet_tcp_cc_fse, OID_AUTO, aggregate_idx, CTLFLAG_RW | CTLFLAG_VNET,
	   &VNET_NAME(cc_fse_aggregate_idx), 0, "idx");

SYSCTL_PROC(_net_inet_tcp_cc_fse, OID_AUTO, default_prio,
	    CTLFLAG_RW | CTLFLAG_VNET | CTLTYPE_UINT, NULL, 0,
	    cc_fse_default_prio_handler, "UI", "default priority");

SYSCTL_PROC(_net_inet_tcp_cc_fse, OID_AUTO, rd,
	    CTLFLAG_RD | CTLFLAG_VNET | CTLTYPE_UINT, NULL, 0,
	    sysctl_aggregate_read, "UI", "dummy value");

SYSCTL_PROC(_net_inet_tcp_cc_fse, OID_AUTO, wr,
	    CTLFLAG_WR | CTLFLAG_VNET | CTLTYPE_UINT, NULL, 0,
	    sysctl_aggregate_write, "UI", "dummy value");


