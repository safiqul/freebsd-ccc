/*-
 * Copyright (c) 2015, 2016 Kristian A. Hiorth <kah@kahnews.net>
 * Copyright (c) 2016, Øystein Dale
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _NETINET_CC_FSE_H_
#define _NETINET_CC_FSE_H_

#include <sys/queue.h>
#include <sys/lock.h>
#include <sys/rwlock.h>

/* Maximum no. of flows in one FG */
#define FSE_FG_MAX_FLOWS	64

LIST_HEAD(fse_flow_head, fse_flow);
LIST_HEAD(fse_fg_head, fse_fg);

VNET_DECLARE(uint8_t, cc_fse_default_prio);
#define V_cc_fse_default_prio VNET(cc_fse_default_prio)

/* Global FSE state info */
struct fse_state {
	uint64_t fse_next_fid;
	struct fse_fg_head *fse_fgtable;
	u_long fse_fgtable_mask;

	struct rwlock fse_statelock;
};

#define FSE_STATE_RLOCK(fse)		rw_rlock(&(fse).fse_statelock)
#define FSE_STATE_WLOCK(fse)		rw_wlock(&(fse).fse_statelock)
#define FSE_STATE_RUNLOCK(fse)		rw_runlock(&(fse).fse_statelock)
#define FSE_STATE_WUNLOCK(fse)		rw_wunlock(&(fse).fse_statelock)
#define FSE_STATE_UNLOCK(fse)		rw_unlock(&(fse).fse_statelock)
#define	FSE_STATE_TRY_UPGRADE(fse)	rw_try_upgrade(&(fse).fse_statelock)
#define	FSE_STATE_DOWNGRADE(fse)	rw_downgrade(&(fse).fse_statelock)
#define FSE_STATE_LOCK_ASSERT(fse)	rw_assert(&(fse).fse_statelock, RA_LOCKED)
#define FSE_STATE_RLOCK_ASSERT(fse)	rw_assert(&(fse).fse_statelock, RA_RLOCKED)
#define FSE_STATE_WLOCK_ASSERT(fse)	rw_assert(&(fse).fse_statelock, RA_WLOCKED)
#define FSE_STATE_UNLOCK_ASSERT(fse)	rw_assert(&(fse).fse_statelock, RA_UNLOCKED)

/* Flow Group */
struct fse_fg {
	uint32_t fg_id; /* Flow group id */
	u_long fg_s_cwnd; 	/* Aggregate congestion window */
	u_long fg_s_ssthresh;	/* Aggregate ssthresh */
	uint32_t fg_s_p;		/* Sum of priorities */
	uint32_t fg_num_coupled;	/* Number of coupled flows */
	/* Timestamp of last event affecting this FG: */
	struct timeval fg_lastupdate;
	struct fse_flow *fg_coco; /* CoCo flow (aggr. leader) */

	/* Back-pointers to all member flows: */
	struct rwlock fg_memberslock;
	struct fse_flow_head fg_members;
	uint32_t fg_membercount;

	/* List hook for FG hashtable: */
	LIST_ENTRY(fse_fg) fg_fgte;
};

#define FSE_FG_ADDRHASH(faddr, laddr, mask) 	((faddr ^ laddr) & (mask))

#define FSE_DEFAULT_PRIORITY	16 /* 2^4, middle of 8 bit range */

/* Flags values for flow state */
#define FSE_FLOW_UNCOUPLED	0x0
#define FSE_FLOW_COUPLED	0x2
#define FSE_FLOW_WANTED_SS	0x4 /* Tried to go to slow start */
#define FSE_FLOW_IN_FR		0x8 /* Currently in fast recovery */

struct fse_flow {
	uint64_t f_fi;		/* Flow Identifier */
	struct fse_fg *f_fg;	/* Containing FG */
	uint8_t f_group_id;		/* Priority */
	uint8_t f_p;		/* Priority */
	u_long f_cwnd;		/* Previously used window */
	u_long f_ssthresh;	/* Previousled used ssthresh */
	uint32_t f_state;	/* Coupling state */

	uint8_t f_ssbit;
	uint8_t f_timeout;

	struct inpcb *f_inp;	/* Pointer to PCB */

	/* List hooks for FG member list: */
	LIST_ENTRY(fse_flow) f_fge;
};

#define FSE_FG_MBR_RLOCK(fg)			rw_rlock(&(fg)->fg_memberslock)
#define FSE_FG_MBR_WLOCK(fg)			rw_wlock(&(fg)->fg_memberslock)
#define FSE_FG_MBR_RUNLOCK(fg)			rw_runlock(&(fg)->fg_memberslock)
#define FSE_FG_MBR_WUNLOCK(fg)			rw_wunlock(&(fg)->fg_memberslock)
#define FSE_FG_MBR_UNLOCK(fg)			rw_unlock(&(fg)->fg_memberslock)
#define	FSE_FG_MBR_TRY_UPGRADE(fg)		rw_try_upgrade(&(fg)->fg_memberslock)
#define	FSE_FG_MBR_DOWNGRADE(fg)		rw_downgrade(&(fg)->fg_memberslock)
#define FSE_FG_MBR_LOCK_ASSERT(fg)		rw_assert(&(fg)->fg_memberslock, RA_LOCKED)
#define FSE_FG_MBR_RLOCK_ASSERT(fg)		rw_assert(&(fg)->fg_memberslock, RA_RLOCKED)
#define FSE_FG_MBR_WLOCK_ASSERT(fg)		rw_assert(&(fg)->fg_memberslock, RA_WLOCKED)
#define FSE_FG_MBR_UNLOCK_ASSERT(fg)	rw_assert(&(fg)->fg_memberslock, RA_UNLOCKED)

/* Flags for rate updates: */
/* ssthresh reset to corrected value (allow increase): */
#define FSE_UPDATE_SSTHRESH_RESET	0x2
#define FSE_UPDATE_INP_UNLOCKED		0x4 /* No INPCB lock held */
#define FSE_UPDATE_CONG_SIGNAL		0x8 /* Cong. signal received */
#define FSE_UPDATE_ACK_RECEIVED		0x10
#define FSE_UPDATE_RTO				0x20
#define FSE_UPDATE_ECN				0x40
#define FSE_UPDATE_AFTER_IDLE		0x80

#define KTR_FSE KTR_SPARE3 	/* Steal SPARE3 class. */

struct fse_flow* cc_fse_register(struct inpcb *inp);
void cc_fse_deregister(struct fse_flow *f);
void cc_fse_update(struct fse_flow *f, uint32_t flags, uint32_t incr);

#endif	/* _NETINET_CC_FSE_H_ */
